const axios = require('axios');

async function getBrasilAbertoData(zipCode) {
  try {
    const response = await axios.get(`https://brasilaberto.com/api/v1/zipcode/${zipCode}`);
    const cep = response.data.result.zipcode;

    const formattedCEP = cep.length === 8 ? `${cep.substr(0, 5)}-${cep.substr(5, 3)}` : cep;

    return {
      cep: formattedCEP,
      logradouro: response.data.result.street,
      complemento: response.data.result.complement,
      bairro: response.data.result.district,
      cidade: response.data.result.city,
      uf: response.data.result.stateShortname,
      //ibge: response.data.result.ibgeId,
    };
  } catch (error) {
    throw new Error(`Error fetching data from BrasilAberto: ${error.message}`);
  }
}

module.exports = getBrasilAbertoData;