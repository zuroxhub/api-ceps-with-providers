const axios = require('axios');

async function getViacepData(zipCode) {
  try {
    const response = await axios.get(`https://viacep.com.br/ws/${zipCode}/json/`);
    
    return {
      cep: response.data.cep,
      logradouro: response.data.logradouro,
      complemento: response.data.complemento,
      bairro: response.data.bairro,
      cidade: response.data.localidade,
      uf: response.data.uf,
      //ibge: response.data.ibge,
      //gia: response.data.gia,
      //ddd: response.data.ddd,
      //siafi: response.data.siafi,
    };
  } catch (error) {
    throw new Error(`Error fetching data from ViaCEP: ${error.message}`);
  }
}

module.exports = getViacepData;