const express = require('express');
const viacepProvider = require('./providers/viacepProvider');
const brasilAbertoProvider = require('./providers/brasilAbertoProvider');
const cors = require('cors');

const app = express();
const port = 3000;
app.use(cors());

const searchCEP = async (req, res, next) => {
  const zipCode = req.params.zipCode;

  console.log('consultando',zipCode)

  const providers = [
    { name: 'ViaCEP', provider: viacepProvider },
    { name: 'BrasilAberto', provider: brasilAbertoProvider },
  ];

  for (const { name, provider } of providers) {
    try {
      const data = await provider(zipCode);
      
      if( typeof data.cep === 'undefined' ) {
        return res.json({
          provider: null,
          error: "CEP Inválido"
        });
      } 

      res.json({
        provider: name,
        ...data,
        //country: 'BR',
      });
      return;
    } catch (error) {
      console.error(`Error fetching data from ${name}:`, error.message);
    }
  }

  res.status(404).json({ error: 'CEP not found' });
};

app.get('/cep/:zipCode', searchCEP);

app.listen(port, () => {
  console.log(`Server is running on port: ${port}`);
});